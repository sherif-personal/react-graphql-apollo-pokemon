const path = require("path");
const commonWebpackConfig = require("./webpack.common");
const mergeWebpackFiles = require("webpack-merge");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");

module.exports = mergeWebpackFiles(commonWebpackConfig, {
    mode: "development",
    output: {
        filename: "[name].bundle.js",
        path: path.resolve(__dirname, "dist")
    },
    devServer: {
        port: 3000,
        hot: true
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: "./src/html/template.html"
        }),
        new webpack.HotModuleReplacementPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ["style-loader", "css-loader", "sass-loader"]
            }
        ]
    }
})