import React, {} from 'react';
import { ApolloClient } from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';

import PokemonsContainer from './containers/PokemonsContainer';

function App() {
  const defaultOptions = {
    watchQuery: { fetchPolicy: 'network-only', errorPolicy: 'all' },
    query: { fetchPolicy: 'network-only', errorPolicy: 'all' },
    mutate: { errorPolicy: 'all' },
  };

  const cache = new InMemoryCache();

  const link = new HttpLink({
    uri: 'https://graphql-pokemon.now.sh',
  });

  const client = new ApolloClient({
    defaultOptions,
    cache,
    link,
  });

  return (
    <ApolloProvider client={client}>
      <main>
        <PokemonsContainer />
      </main>
    </ApolloProvider>
  );
}

export default App;
